#!/bin/python3
from pathlib import Path

import argparse
import re

from todo.TodoObject import TodoObject

from todo import Todo, Category, Task

parser = argparse.ArgumentParser("Todo.md CLI Application", description="Access your todos via the command line!")

parser.add_argument("-md", "--markdown", action="store_true", help="Show markdown.")
parser.add_argument("-cat", "--category", action="store", help="Category the command should target.")
parser.add_argument("-t", "--task", action="store", help="Target a specific task in a category.")
parser.add_argument("-i", "--info", action="store_true", help="Print information about the given object.")
parser.add_argument("-x", "--complete", action="store_true", help="Toggle a task's completed status.")  # x = done, right?
parser.add_argument("-f", "--file", action="store", help="Specify the file to load into a Todo.")

args = parser.parse_args()

if args.file is not None:
    todo = Todo(Path(args.file))
else:
    todo = Todo()

def print_incomplete_tasks(data: TodoObject):
    if len(data.get_tasks(complete=False)) > 0:
        print("\n--- Incomplete Task(s) ---")
        for category in [data, *data.get_categories()]:  # allows us to catch tasks in subcategories and the main data object.
            if len(category.get_tasks(immediate=False, complete=False)) > 0:
                print(f"\n{category}")
                for task in category.get_tasks(immediate=True, complete=False):
                    print(task)
                    if task.has_children:
                        print(task.get_md())

data = todo  # start with base todo object as input data.

#  --category handler
if args.category is not None:
    args.category = f".*{args.category}"
    data = data.get_category(args.category)

#  --task handler
if args.task is not None:
    args.task = f".*{args.task}"
    data = data.get_task(args.task)

#  --done handler
if args.complete:
    if isinstance(data, Task):
        data.toggle_complete()
        print(f"Set complete status for '{data.text}' to '{data.complete}'.")
        todo.write_data()
    else:
        print("-d is only applicable to tasks.")

## --markdown handler
if args.markdown:
    print(data)
    print(data.get_md())

## --info handler
if args.info:
    task_count = len(data.get_tasks())
    incomplete_count = len(data.get_tasks(complete=False))
    if isinstance(data, Todo):
        print(data)
        print("Categories: ", ", ".join([x.text for x in data.get_children(obj_type=Category)]))
        print(f"Tasks: \n\tTotal: {task_count}\n\tIncomplete: {incomplete_count}")
        print_incomplete_tasks(data)
    elif isinstance(data, Category):
        print("Category:", data.text)
        print(f"Tasks: \n\tTotal: {task_count}\n\tIncomplete: {incomplete_count}")
        print_incomplete_tasks(data)
    elif isinstance(data, Task):
        print("Category: ", data.task_category().text)
        print("Task:", data.text)
        if data.date is not None:
            print("Due Date:", data.date.strftime("%b %d %Y"))
        print("Complete:", data.complete)
    else:
        print("Don't know how we ended up here. :/")
