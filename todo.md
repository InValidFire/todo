# todo.md

this is the to-do list for the `todo.md` project. *woah*.

## spec
- one line represents an object in `todo.md`
- an object can either be a category header, a task, or a note.
- category headers can go as many levels as necessary.
- tasks can have due-dates, and they can either be complete, or incomplete.
  - [ ] this is an incomplete task.
  - [x] this is a complete task with a due date |Sep 30 2021
    - [x] tasks can be nested on other tasks or notes.
- [ ] or they can be on the root of the category.
  - all tasks must belong under a category though.
    - so no line 1 tasks.

## features
- [ ] add recurring tasks
  - thinking about having the following section for spec
- [x] add tasks
- [x] add notes
  - notes can be listed or unlisted.
    - listed notes are like these here.
    - unlisted notes is what's on line 3.
    - and clearly, they can be nested.

## recurring
- some recurring task |how often it should repeat |the last time completed
  - possible values for repeat time:
    - daily
    - weekly - mon
    - biweekly - wed
    - monthly - 15
	- yearly - jan - 17
