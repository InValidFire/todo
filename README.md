# todo.py

todo.py is my attempt to organize myself and get things straight. I disliked the specification declared by the folks at [todo-md](https://www.github.com/todo-md/todo-md), so naturally, I'm making my own.

## Specification

```md
# todo.md

## Section

- [ ] some incomplete task with a due date |Sep 24 2021
- [x] some complete task without a due date

### Sub-Section
- [ ] here's another task with a due date |Oct 13 2021
```
