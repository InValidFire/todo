from typing import Optional

from .TodoObject import TodoObject

class Note(TodoObject):
    def __init__(self, level: int, text: str, parent: TodoObject = None, listed: bool = True):
        self.listed = listed
        super().__init__(level, text, parent)

    def __str__(self):
        if self.listed:
            return f"- {self.text}"
        else:
            return self.text
