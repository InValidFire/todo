from __future__ import annotations

from .TodoObject import TodoObject

class Category(TodoObject):
    def __init__(self, name : str, level: int, parent = None):
        super().__init__(level, name, parent)
    
    def __str__(self):
        return "#"*(self.level+1) + f" {self.text}"
