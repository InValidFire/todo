from __future__ import annotations

from typing import Union
from datetime import datetime

from .TodoObject import TodoObject

class Task(TodoObject):
    def __init__(self, name : str, date = Union[datetime, None], complete: bool = False, level: int = 0, parent = None):
        self.date = date
        self.complete = complete
        super().__init__(level, name, parent)

    def task_category(self) -> Category:
        """Get the category this task belongs to.

        Returns:
            Category: The category this task belongs to.
        """
        return self.get_parents(Category)[-1]

    def toggle_complete(self):
        """Toggle this task's complete value."""
        self.complete = not self.complete
        
    def __str__(self):
        output = ""
        if self.complete:
            output += "- [x]"
        else:
            output += "- [ ]"
        if self.date is not None:
            self.date: datetime
            output += f" {self.text} |{self.date.strftime('%b %d %Y')}"
        else:
            output += f" {self.text}"
        return output

from .Category import Category
